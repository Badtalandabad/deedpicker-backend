const express = require('express');
const router = express.Router();
const ActivitiesController = require('../core/controllers/activitiesController');
const DaysController = require('../core/controllers/daysController');

const activitiesController = new ActivitiesController();
const daysController = new DaysController();

router.post('/get-all-activities', activitiesController.getAllActivities.bind(activitiesController));
router.post('/edit-activity', activitiesController.editActivity.bind(activitiesController));
router.post('/add-activity', activitiesController.addActivity.bind(activitiesController));
router.post('/delete-activity', activitiesController.deleteActivity.bind(activitiesController));

router.post('/get-days-in-range', daysController.getDaysInRange.bind(daysController));
router.post('/checkout-day', daysController.checkoutDay.bind(daysController));

module.exports = router;
