let config = {
  db: {
    host: 'localhost',
    port: 'port',
    user: 'user',
    password: 'password',
    database: 'database name',
  },
};

module.exports = config;