const DaysModel = require('../models/daysModel');
const BaseController = require('./baseController');
const {errorHandler} = require('../library/errorHandler');
const {responseHandler} = require('../library/responseHandler');

/**
 * Days controller
 */
class DaysController extends BaseController {
  /**
   * Model
   *
   * @type {DaysModel}
   */
  _model;

  /**
   * Constructor. Initializes model
   */
  constructor() {
    super();

    this._model = new DaysModel();
  }

  /**
   * Retrieves all days data in a certain range
   *
   * @param req Request object
   * @param res Response object
   */
  getDaysInRange(req, res) {
    const dateStart = req.body.dateStart;
    const dateEnd = req.body.dateEnd;
    const activityId = req.body.activityId;

    if ([dateStart, dateEnd, activityId].includes(undefined)) {
      responseHandler.respondBadRequest(res, 'Parameters: dateStart, dateEnd, activityId are mandatory.');
      return;
    }

    this._model.getDaysInRange(dateStart, dateEnd, activityId)
      .then(queryResult => {
        if (!queryResult) {
          throw new Error('Failed to retrieve days from database');
        }
        const daysHashArray = {};
        queryResult.forEach(day => {
          if (!(day.date instanceof Date)) {
            throw new Error('Wrong date format in database');
          }
          day.date.setHours(0, 0, 0, 0);
          daysHashArray[day.date.getTime()] = day;
        });

        responseHandler.respondOk(res, daysHashArray);
      }).catch(error => errorHandler.finishRequestWithError(error, req, res));
  }

  /**
   * Checks out specific day in the database
   *
   * @param req Request object
   * @param res Response object
   */
  checkoutDay(req, res) {
    const date = req.body.date;
    const activityId = req.body.activityId;
    const activityStatus = req.body.activityStatus;

    if ([date, activityId, activityStatus].includes(undefined)) {
      responseHandler.respondBadRequest(res, 'Parameters: date, activityId, activityStatus are mandatory.');
      return;
    }

    this._model.checkoutDay(date, activityId, activityStatus)
      .then(result => {
        if (!result) {
          throw new Error('Failed to checkout');
        }
        responseHandler.respondOk(res, result);
      }).catch(error => errorHandler.finishRequestWithError(error, req, res));
    //TODO: add catch for wrong arguments
  }
}

module.exports = DaysController;