const BaseController = require('./baseController');
const ActivitiesModel = require('../models/activitiesModel');
const {errorHandler} = require('../library/errorHandler');
const {responseHandler} = require('../library/responseHandler');

/**
 * Activities Controller
 */
class ActivitiesController extends BaseController {
  /**
   * Model
   *
   * @type {ActivitiesModel}
   */
  _model;

  /**
   * Constructor. Initializes model
   */
  constructor() {
    super();

    this._model = new ActivitiesModel();
  }

  /**
   * Sends all events from database
   *
   * @param req Request object
   * @param res Response object
   */
  getAllActivities(req, res) {
    this._model.getAllActivities()
      .then(queryResult => {
        if (!queryResult) {
          throw new Error('Failed to get all activities');
        }
        const activitiesHashArray = {};
        queryResult.forEach(activity => {
          activitiesHashArray[activity._id] = activity;
        });
        responseHandler.respondOk(res, activitiesHashArray);
      }).catch(error => errorHandler.finishRequestWithError(error, req, res));
  }

  /**
   * Edits activity
   *
   * @param req Request object
   * @param res Response object
   */
  editActivity(req, res) {
    const activityId = req.body.activityId;
    const activityName = req.body.activityName;
    if ([activityId, activityName].includes(undefined)) {
      responseHandler.respondBadRequest(res, 'Parameters: activityId, activityName are mandatory.');
      return;
    }

    this._model.modifyActivity(activityName, activityId)
      .then(result => {
        if (!result) {
          throw new Error(`Failed to edit activity, id: ${activityId} name: ${activityName}`);
        }
        responseHandler.respondOk(res, true);
      }).catch(error => errorHandler.finishRequestWithError(error, req, res));
  }

  /**
   * Saves new activity
   *
   * @param req Request object
   * @param res Response object
   */
  addActivity(req, res) {
    const activityName = req.body.activityName;
    if (activityName === undefined) {
      responseHandler.respondBadRequest(res, 'Parameter activityName is mandatory.');
      return;
    }

    this._model.saveNewActivity(activityName)
      .then(activityId => {
        if (!activityId) {
          throw new Error(`Failed save activity with the name: ${activityName}`);
        }
        responseHandler.respondOk(res, activityId);
      }).catch(error => errorHandler.finishRequestWithError(error, req, res));
  }

  /**
   * Deletes activity with all checked out days
   *
   * @param req Request object
   * @param res Response object
   */
  deleteActivity(req, res) {
    const activityId = req.body.activityId;
    if (activityId === undefined) {
      responseHandler.respondBadRequest(res, 'Parameter activityId is mandatory.');
      return;
    }

    this._model.deleteActivity(activityId).then(result => {
      if (!result) {
        throw new Error(`Failed to delete activity, id: ${activityId}`);
      }
      responseHandler.respondOk(res, true);
    }).catch(error => errorHandler.finishRequestWithError(error, req, res));
  }
}

module.exports = ActivitiesController;