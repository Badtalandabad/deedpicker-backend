const BaseDocument = require('./baseDocument.js');

class ActivityDocument extends BaseDocument {
  /**
   * Activity name
   *
   * @type {string}
   */
  _name = '';

  /**
   * Returns activity name
   *
   * @return {string}
   */
  get name() {
    return this._name;
  }

  /**
   * Sets activity name
   *
   * @param name Activity name
   */
  set name(name) {
    this._name = name;
  }

  constructor(name) {
    super();
    this._name = name;
  }
}

module.exports = ActivityDocument;