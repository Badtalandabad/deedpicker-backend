module.exports = Object.freeze({
  ID_FIELD: '_id',
  CREATED_AT_FIELD: 'createdAt',
  UPDATED_AT_FIELD: 'updatedAt',

  ACTIVITIES_COLLECTION:    'activities',
  ACTIVITIES_NAME_FIELD:    'name',

  DAYS_COLLECTION: 'days',
  DAYS_DATE_FIELD: 'date',
  DAYS_ACTIVITY_STATUSES_FIELD: 'activityStatuses', //associative array with activity statuses

});