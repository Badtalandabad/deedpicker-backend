const BaseDocument = require('./baseDocument');

/**
 * Date entity
 */
class DayDocument extends BaseDocument {
  /**
   * Date when event happened
   *
   * @type {Date}
   * @private
   */
  _date;

  /**
   * Statuses of different activities on this day.
   * {activityId: boolean status}
   *
   * @type {Object}
   * @private
   */
  _activityStatuses = {};

  /**
   * Returns event date
   *
   * @return {Date}
   */
  get date() {
    return this._date;
  }

  /**
   * Sets event date
   *
   * @param date Date when event happened
   */
  set date(date) {
    if (!(date instanceof Date)) {
      date = new Date(date);
    }
    this._date = date;
  }

  /**
   * Returns activity statuses associative array
   *
   * @return {Object}
   */
  get activityStatuses() {
    return this._activityStatuses;
  }

  /**
   * Sets activity statuses associative array
   * {activityId: activity status}
   *
   * @param statuses
   */
  set activityStatuses(statuses) {
    this._activityStatuses = statuses;
  }

  /**
   * Returns true if event happened on this day
   *
   * @param activityId Id of the activity
   *
   * @return {Boolean}
   */
  getActivityStatus(activityId) {
    if (activityId in this._activityStatuses && !this._activityStatuses.hasOwnProperty(activityId)) {
      throw new Error('Unacceptable activity id value');
    }
    return !!this._activityStatuses[activityId];
  }

  /**
   * Adds event as a field in the current object
   * Sets it true if event happened on this date, false - otherwise
   *
   * @param activityId Id of the event
   * @param status     Status of the event, true if it happened, false otherwise
   */
  setActivityStatus(activityId, status) {
    if (activityId in this._activityStatuses && !this._activityStatuses.hasOwnProperty(activityId)) {
      throw new Error('Unacceptable activity id value');
    }
    this._activityStatuses[activityId] = status;
  }

  /**
   * Constructor
   *
   * @param date             Date of the day
   * @param activityStatuses Statuses of activity happened on this day {activityId: activityStatus}
   */
  constructor(date, activityStatuses) {
    super();
    this._date = date;
    this._activityStatuses = activityStatuses;
  }
}

module.exports = DayDocument;