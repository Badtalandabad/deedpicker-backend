const dbNames = require('./collectionNames');

/**
 * Basic functionality for all documents
 */
class BaseDocument {
  /**
   * Object id
   *
   * @type {string}
   */
  _id;

  /**
   * Creation time
   *
   * @type {Date}
   */
  _createdAt;

  /**
   * Last update time
   *
   * @type {Date}
   */
  _updatedAt;

  /**
   * Returns id
   *
   * @return {string}
   */
  get id() {
    return this._id;
  }

  /**
   * Sets id
   *
   * @param id
   */
  set id(id) {
    this._id = id;
  }

  /**
   * Returns creation time
   *
   * @return {Date}
   */
  get createdAt() {
    return this._createdAt;
  }

  /**
   * Sets creation time
   *
   * @param time Crated at date object
   */
  set createdAt(time) {
    this._createdAt = time;
  }

  /**
   * Returns last update time
   *
   * @return {Date}
   */
  get updatedAt() {
    return this._updatedAt;
  }

  /**
   * Sets last update time
   *
   * @param time Updated at Date object
   */
  set updatedAt(time) {
    this._updatedAt = time;
  }

  prepareForDb(removeId = true) {
    const objectForDb = {};
    for (let [key, value] of Object.entries(this)) {
      if (typeof value === 'function' || removeId && key === dbNames.ID_FIELD) {
        continue;
      }
      let dbKey = key.replace(/^_/, '');
      objectForDb[dbKey] = value;
    }

    return objectForDb;
  }
}

module.exports = BaseDocument;