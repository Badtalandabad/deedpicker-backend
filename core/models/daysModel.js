const {db} = require('../library/dbHandler');
const {errorHandler} = require('../library/errorHandler');
const dbNames = require('../dbCollections/collectionNames');
const DayDocument = require('../dbCollections/dayDocument');
const BaseModel = require('./baseModel');

/**
 * Days model
 */
class DaysModel extends BaseModel {
  /**
   * Collection name
   */
  collectionName = dbNames.DAYS_COLLECTION;

  _validateDate(dateToCheck) {
    const dateRegExp = /^\d{4}-\d{2}-\d{2}$/;

    return dateRegExp.test(dateToCheck);
  }

  /**
   * Retrieves days in given range
   *
   * @param dateStart  Start date
   * @param dateEnd    End date
   * @param activityId Activity id string
   *
   * @return {Promise<Object>}
   */
  getDaysInRange(dateStart, dateEnd, activityId) {
    if (!this._validateDate(dateStart)
      || !this._validateDate(dateEnd)
      || typeof activityId !== "string"
    ) {
      throw new Error(`Invalid parameters ${dateStart}, ${dateEnd}, ${activityId}`);
    }

    const condition = {
      date: {
        $gte: new Date(dateStart),
        $lte:  new Date(dateEnd),
      }
    };

    return db.findMany(this.collectionName, condition);
  }

  /**
   * Checks out specific day
   *
   * @param date           Date
   * @param activityId     Activity id string
   * @param activityStatus Status of the day
   *
   * @return {Promise<Object>}
   */
  checkoutDay(date, activityId, activityStatus) {
    if (!this._validateDate(date)
      || typeof activityId !== "string"
      || typeof activityStatus !== "boolean"
    ) {
      throw new Error(`Invalid parameters ${date}, ${activityId}, ${activityStatus}`);
    }

    //TODO: change format
    date = new Date(date + 'T00:00:00Z');

    const returnPromise = db.findOne(
      dbNames.ACTIVITIES_COLLECTION,
      {[dbNames.ID_FIELD]: db.prepareIdValue(activityId)}
      ).then(result => {
        if (!result) {
          throw new Error('No activity with the id: ' + activityId);
        }
      }).then(()=> {
        const condition = {[dbNames.DAYS_DATE_FIELD]: date,};
        return db.findOne(this.collectionName, condition);
      }).then(dayQueryResult => {
        if (dayQueryResult) {
          const day = new DayDocument(dayQueryResult.date, dayQueryResult.activityStatuses);
          if (day.getActivityStatus(activityId) !== activityStatus) {
            throw new Error('Data in storage is inconsistent with client');
          }
          day.setActivityStatus(activityId, !day.getActivityStatus(activityId));
          const updateCondition = {[dbNames.DAYS_DATE_FIELD]: date};
          const newValues = {[dbNames.DAYS_ACTIVITY_STATUSES_FIELD]: day.activityStatuses};
          return db.updateOne(this.collectionName, newValues, updateCondition)
            .then(result => result.result.nModified)
            .catch(errorHandler.logError);
        } else {
          const newDocument = new DayDocument(date, {[activityId]: true});
          return db.insertOne(this.collectionName, newDocument)
            .then(result => {
              return result.insertedId;
            }).catch(errorHandler.logError);
        }
      });

    return returnPromise;
  }

}

module.exports = DaysModel;