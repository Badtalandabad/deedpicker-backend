const BaseModel = require('./baseModel');
const {db} = require('../library/dbHandler');
const dbNames = require('../dbCollections/collectionNames');
const ActivityDocument = require('../dbCollections/activityDocument');

/**
 * Activities model
 */
class ActivitiesModel extends BaseModel {
  /**
   * Collection  name
   */
  collectionName = dbNames.ACTIVITIES_COLLECTION;

  /**
   * Returns all activities
   *
   * @return {Promise<Object>}
   */
  getAllActivities() {
    return db.findMany(this.collectionName);
  }

  /**
   * Updates activity document
   *
   * @param activityId   Activity id
   * @param activityName Activity name
   *
   * @return {Promise<Number>}
   */
  modifyActivity(activityId, activityName) {
    if (typeof activityId !== 'string' || typeof activityName !== 'string') {
      throw new Error(`Invalid parameters: activityName ${activityName}, activityId ${activityId}`);
    }

    const condition = {_id: db.prepareIdValue(activityId)};
    return db.updateOne(this.collectionName, {name: activityName}, condition)
      .then(queryResult => queryResult.result.nModified);
  }

  /**
   * Adds new activity document to the database
   *
   * @param activityName Activity name
   *
   * @return {Promise<ObjectId>}
   */
  saveNewActivity(activityName) {
    if (typeof activityName !== 'string') {
      throw new Error(`Invalid parameters: activityName ${activityName}`);
    }

    const newDocument = new ActivityDocument(activityName);
    return db.insertOne(this.collectionName, newDocument)
      .then(queryResult => queryResult.insertedId);
  }

  /**
   * Deletes activity and all its checked out days in database
   *
   * @param activityId Activity id
   *
   * @return {Promise<Object>}
   */
  deleteActivity(activityId) {
    if (typeof activityId !== "string") {
      throw new Error(`Invalid parameter: activityId ${activityId}`);
    }

    return db.deleteOne(this.collectionName, {_id: db.prepareIdValue(activityId)})
      .then(queryResult => {
        if (!queryResult || !queryResult.result.n) {
          throw new Error('Deleting activity failed');
        }
        const options = {
          $unset: {
            [dbNames.DAYS_ACTIVITY_STATUSES_FIELD + '.' + activityId]: 1
          }
        };
        return db.updateMany(dbNames.DAYS_COLLECTION, {}, {}, options, false);
      }).then(queryResult => queryResult.result.ok);
  }
}

module.exports = ActivitiesModel;