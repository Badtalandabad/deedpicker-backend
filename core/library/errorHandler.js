const {responseHandler} = require('./responseHandler');

/**
 * Error Handler. Processes and reports errors.
 */
class ErrorHandler {
  /**
   * Logs error
   *
   * @param error Error object
   */
  logError(error) {
    //TODO: implement proper logging
    console.log(error);
  }

  /**
   * Logs error, sends general error message with status 500
   * Is an arrow function in order to make it suitable as middleware
   *
   * @param error           Error object
   * @param expressRequest  Express.js request object
   * @param expressResponse Express.js response object
   * @param next            Next function in middleware
   */
  finishRequestWithError(error, expressRequest, expressResponse, next) {
    expressResponse.locals.error = error;
    const status = error.status || 500;

    this.logError(error);

    let message = 'Error';
    switch (status) {
      case 404:
        message = 'Not found';
        break;
      case 500:
        message = 'Internal server error';
        break;
    }

    responseHandler.respondToClient(expressResponse, status, null, message);
  }
}

module.exports = {
  ErrorHandler,
  errorHandler: new ErrorHandler(),
};