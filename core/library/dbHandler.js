const mongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

/**
 * Database handler. Provides interaction with MongoDB
 */
class DbHandler {
  /**
   * Database connection
   *
   * @type {MongoClient}
   */
  dbClient;

  /**
   * Database name
   *
   * @type {string}
   */
  dbName;

  /**
   * Db object initialization
   */
  constructor() {

  }

  /**
   * Connects to the database and executes callback on success
   *
   * @param host     Host
   * @param port     Port
   * @param user     Database user name
   * @param password Database user password
   * @param dbName   Database name
   *
   * @return {Promise<MongoClient>}
   */
  connect(host, port, user, password, dbName) {
    const connectionUrl = `mongodb://${user}:${password}@${host}:${port}/${dbName}`;

    return mongoClient.connect(connectionUrl, {useUnifiedTopology: true})
      .catch(error => {console.log(error)});
  }

  /**
   * Db object initialization
   */
  init(dbConfig) {
    const host = dbConfig.host;
    const port = dbConfig.port;
    const user = dbConfig.user;
    const password = dbConfig.password;
    this.dbName = dbConfig.database;

    this.connect(host, port, user, password, this.dbName).then(client => {this.dbClient = client});
  }

  prepareIdValue(id) {
    return ObjectId(id);
  }

  /**
   * Finds given collection and passes it to the given callback
   *
   * @param collectionName        Collection name
   * @param callbackForCollection Function to call if collection is found
   *
   * @return {Promise<Object>}
   */
  useCollection(collectionName, callbackForCollection) {
    const dbQueryCallback = (resolve) => {
      const db = this.dbClient.db(this.dbName);
      if (!db) {
        throw new Error('Database is not found');
      }

      db.collection(collectionName, {strict: true}, (error, collection) => {
        if (error) {
          throw error;
        }
        callbackForCollection(collection, resolve);
      });
    };

    return new Promise(dbQueryCallback);
  }

  /**
   * Finds one document in given collection
   *
   * @param collectionName Name of the collection
   * @param condition      Condition for a search
   *
   * @returns {Promise<Object>}
   */
  findOne(collectionName, condition = {}) {
    const findDocument = (collection, resolve) => {
      collection.findOne(condition, (error, item) => {
        if (error) {throw error;}
        resolve(item);
      });
    };

    return this.useCollection(collectionName, findDocument);
  }

  /**
   * Finds multiple documents in given collection
   *
   * @param collectionName
   * @param condition
   *
   * @return {Promise<Object>}
   */
  findMany(collectionName, condition = {}) {
    const findDocuments = (collection, resolve) => {
      collection.find(condition).toArray((error, items) => {
        if (error) {throw error;}
        resolve(items);
      });
    };

    return this.useCollection(collectionName, findDocuments);
  }

  /**
   * Updates one document in given collection
   *
   * @param collectionName Collection name
   * @param setValues      Fields to update with new values
   * @param condition      Match criteria
   * @param options        Update query options
   * @param updateValuesViaSetOption True - update only given fields, false rewrite the whole document
   *
   * @return {Promise<Object>}
   */
  updateOne(collectionName, setValues, condition, options = {}, updateValuesViaSetOption = true) {
    const updateDocument = (collection, resolve) => {
      if (updateValuesViaSetOption){
        options.$set = setValues;
      }
      if (options.$currentDate === undefined) {
        options.$currentDate = {updatedAt: true};
      }
      if (!condition) {
        throw new Error('Update query no condition provided');
      }

      collection.updateOne(condition, options, (error, result) => {
        if (error) {throw error;}
        resolve(result);
      });
    };

    return this.useCollection(collectionName, updateDocument);
  }

  /**
   * Updates several documents in given collection
   *
   * @param collectionName Collection name
   * @param setValues      Fields to update with new values
   * @param condition      Match criteria
   * @param options        Update query options
   * @param updateValuesViaSetOption True - update only given fields, false rewrite the whole document
   *
   * @return {Promise<Object>}
   */
  updateMany(collectionName, setValues, condition, options = {}, updateValuesViaSetOption = true) {
    const updateDocuments = (collection, resolve) => {
      if (updateValuesViaSetOption){
        options.$set = setValues;
      }
      if (options.$currentDate === undefined) {
        options.$currentDate = {updatedAt: true};
      }
      if (!condition) {
        throw new Error('Update query no condition provided');
      }

      collection.updateMany(condition, options, (error, result) => {
        if (error) {throw error;}
        resolve(result);
      });
    };

    return this.useCollection(collectionName, updateDocuments);
  }

  /**
   * Inserts one document into given collection
   *
   * @param collectionName Collection name
   * @param document       Document to insert
   *
   * @return {Promise<Object>}
   */
  insertOne(collectionName, document) {
    const insertDocument = (collection, resolve) => {
      document.createdAt = new Date();
      document.updatedAt = document.createdAt;
      if (document.prepareForDb !== undefined) {
        document = document.prepareForDb();
      }
      collection.insertOne(document, (error, result) => {
        if (error) {throw error;}
        resolve(result);
      });
    };

    return this.useCollection(collectionName, insertDocument)
  }

  /**
   * Deletes one document from collection
   *
   * @param collectionName Name of the collection
   * @param condition      Condition for deleting document
   *
   * @return {Promise<Object>}
   */
  deleteOne(collectionName, condition) {
    const deleteDocument = (collection, resolve) => {
      collection.deleteOne(condition, (error, result) => {
        if (error) {throw error;}
        resolve(result);
      });
    };

    return this.useCollection(collectionName, deleteDocument);
  }

}

module.exports = {
  DbHandler,
  db: new DbHandler(),
};