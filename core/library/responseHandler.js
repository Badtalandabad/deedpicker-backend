/**
 * Helper for processing responding to client
 */
class ResponseHandler {
  /**
   * Responds to client in json format
   *
   * @param expressResponse Express.js response object
   * @param httpStatusCode  Http status code
   * @param requestResult   Result data of the request. Optional
   * @param message         Message. Optional
   */
  respondToClient(expressResponse, httpStatusCode, requestResult = null, message = null) {
    expressResponse.status(httpStatusCode);

    if (!requestResult && !message) {
      expressResponse.end();
      return;
    }

    const responseObject = {};
    if (requestResult) {
      responseObject.result = requestResult;
    }
    if (message) {
      responseObject.message = message;
    }

    expressResponse.json(responseObject);
  }

  /**
   * Responds with 200 status
   *
   * @param expressResponse Express.js response object
   * @param requestResult   Result data of the request. Optional
   * @param message         Message. Optional
   */
  respondOk(expressResponse, requestResult = null, message = null) {
    this.respondToClient(expressResponse, 200, requestResult, message);
  }

  /**
   * Responds with 400 status
   *
   * @param expressResponse Express.js response object
   * @param message         Error message
   */
  respondBadRequest(expressResponse, message) {
    this.respondToClient(expressResponse, 400, null, message);
  }

  /**
   * Responds with 500 status
   *
   * @param expressResponse Express.js response object
   * @param message         Error message
   */
  respondError(expressResponse, message) {
    this.respondToClient(expressResponse, 500, null, message);
  }

}

module.exports = {
  ResponseHandler,
  responseHandler: new ResponseHandler(),
};