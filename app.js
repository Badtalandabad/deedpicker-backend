const express = require('express');
//const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const config = require('./config/config');
const {db} = require('./core/library/dbHandler');
const {errorHandler} = require('./core/library/errorHandler');
const apiV1Router = require('./routes/apiV1Routes');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

if (process.env.NODE_ENV === 'development') {
  app.use(cors());
}

try {
  db.init(config.db);
} catch (error) {
  errorHandler.logError(error);
}

app.use('/api/v1', apiV1Router);

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});
app.use(errorHandler.finishRequestWithError.bind(errorHandler));

module.exports = app;
